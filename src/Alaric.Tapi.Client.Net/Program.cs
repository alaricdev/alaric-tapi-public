﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Alaric.Tapi.Validation;
using Alaric.Tapi.WebSocket;
using Colorful;
using IdentityModel.Client;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Debug;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Sharprompt;
using Console = Colorful.Console;
using Dotnet = System.Net.WebSockets;

namespace Alaric.Tapi.Client.Net
{
    class Program
    {

        private static readonly ManualResetEvent ExitEvent = new ManualResetEvent(false);
        private static readonly HttpClient _tokenClient = new HttpClient();
        private static AlaricWebSocketClient _client;
        private static string _accessToken;
        private static DateTime _expiry;
        private volatile static string _refreshToken;
        private static DiscoveryCache _cache;
        private static DiscoveryDocumentResponse _disco;

        private static Settings _settings;

        private static IOfflineTokenValidator _validator;

        static async Task Main(string[] args)
        {
            AppDomain.CurrentDomain.ProcessExit += CurrentDomainOnProcessExit;
            Console.CancelKeyPress += ConsoleOnCancelKeyPress;

            var styleSheet = CreateOptionsStyleSheet();

            PromptHeader();

            LoadSettings();

            if (!ValidateSettings())
            {
                Console.WriteLine("Terminating application! Please click Ctrl+C", Color.Red);
                ExitEvent.WaitOne();
                return;
            }

            PromptSettings();

            if (string.IsNullOrWhiteSpace(_settings.Username) || string.IsNullOrWhiteSpace(_settings.Password))
            {
                PromptCredentials();
            }

            if (!await CheckDisco().ConfigureAwait(false))
            {
                Console.WriteLine("Terminating application! Please click Ctrl+C", Color.Red);
                ExitEvent.WaitOne();
                return;
            }

            PromptDisco();

            var factory = new Func<Dotnet.ClientWebSocket>(() =>
                {
                    var client = new Dotnet.ClientWebSocket
                    {
                        Options =
                        {
                                KeepAliveInterval = TimeSpan.FromSeconds(60)
                        }
                    };

                    return client;
                });

            var address = new Uri(_settings.TapiUrl);
            var logger = LoggerFactory
                .Create(x => x.AddConsole()
                              .AddFilter<DebugLoggerProvider>(null, LogLevel.Trace)
                              .AddFilter<DebugLoggerProvider>(null, LogLevel.Error))
                .CreateLogger<AlaricWebSocketClient>();

            using (_client = new AlaricWebSocketClient(address, logger, factory))
            {
                _client.Name = "TAPI";
                _client.ReconnectTimeout = TimeSpan.FromSeconds(30);
                _client.RetryTimeout = TimeSpan.FromSeconds(30);
                _client.IsReconnectionEnabled = !_settings.IgnoreIdle;
                _client.Reconnected.Subscribe(info =>
                    logger.LogWarning($"Reconnection happened, reason: {info.Reason}, address: {_client.Address}"));
                _client.Disconnected.Subscribe(info =>
                    logger.LogWarning($"Disconnection happened, reason: {info.Reason}, address: {_client.Address}"));
                _client.ResponseMessage.Subscribe(response =>
                {
                    Console.WriteLine("Response Received:", ColorTranslator.FromHtml("#00b9f1"));
                    Console.WriteLine($"{JObject.Parse(response.Text)}", Color.Green);
                });

                logger.LogInformation("TAPI Starting..");
                _client.Start().Wait();
                logger.LogInformation("TAPI Started!");

                await RetrieveToken().ConfigureAwait(false);

                await Interact(styleSheet, _client).ConfigureAwait(false);

                Console.WriteLine("Quitting...", Color.Yellow);
            }
            ExitEvent.Set();
            ExitEvent.WaitOne();
        }

        private static async Task Interact(StyleSheet styleSheet, IWebSocketClient client)
        {
            var quit = false;

            _ = Task.Run(async () => await RunRefreshTokenAsync(30000));

            do
            {
                Console.WriteLineStyled(new string('=', 75), styleSheet);

                Console.WriteLineStyled(@"
    (P) - Ping TAPI server
    (L) - Login TAPI
    (G) - Get Orders/Balances/Positions
    (O) - New Order
    (U) - Update credentials
    (Q) - Quit", styleSheet);
                Console.WriteLine();

                var input = Prompt.Input<string>(
                    "Please select an option",
                    "Q",
                    new[]
                    {
                        PromptValidator.RegularExpression("[PLGOUQplgouq]"),
                        PromptValidator.MaxLength(1),
                        PromptValidator.Required()
                    });

                switch (input)
                {
                    case "P":
                    case "p":
                        await PingServer(client);
                        break;
                    case "L":
                    case "l":
                        await LoginServer(client);
                        break;
                    case "G":
                    case "g":
                        var getSelection = Prompt.Select(
                            "Which detail do you want to get?",
                            new[] { "Get Positions", "Get Balances", "Get Orders" });
                        await GetDetails(client, getSelection);
                        break;
                    case "O":
                    case "o":
                        var orderSide = Prompt.Select(
                            "What is your order side?",
                            new[] { "BUY", "SELL", "SELL_SHORT", "Return Back"});

                        if (orderSide == "Return Back") break;

                        var orderType = Prompt.Select(
                            "What is your order type?",
                            new[] { "MARKET", "LIMIT", "STOP", "STOP_LIMIT", "PEGGED", "Return Back" });

                        if (orderType == "Return Back") break;

                        var orderTime = Prompt.Select(
                            "What is your order time in force?",
                            new[] { "DAY", "ON_OPEN", "IOC", "EXTENDED_DAY", "ON_CLOSE", "Return Back" });

                        if (orderTime == "Return Back") break;

                        await NewOrder(client, orderSide, orderType, orderTime);
                        break;
                    case "U":
                    case "u":
                        PromptCredentials();
                        await RetrieveToken().ConfigureAwait(false);
                        break;
                    case "Q":
                    case "q":
                        quit = true;
                        break;
                    default:
                        Console.Write("Invalid option!\n", Color.Red);
                        break;
                }
            }
            while (!quit);
        }

        private static async Task PingServer(IWebSocketClient client)
        {
            if (!client.IsRunning)
            {
                Console.WriteLine("Client is not running or connected, please retry!", Color.Yellow);
            }

            var request = JObject.FromObject(new
            {
                reqId = "PING-TEST",
                messageType = "ping"
            }).ToString();

            client.Send(request);

            Console.WriteLine("Request Sent:", ColorTranslator.FromHtml("#00b9f1"));
            Console.WriteLine($"{request}", Color.Green);

            await Task.Delay(_settings.PrintDelay);
        }

        private static async Task LoginServer(IWebSocketClient client)
        {
            if (!client.IsRunning)
            {
                Console.WriteLine("Client is not running or connected, please retry!", Color.Yellow);
                return;
            }

            if (string.IsNullOrWhiteSpace(_accessToken))
            {
                Console.WriteLine("Login requires a valid access token. Please get an access token bu (U)pdating your credentials!", Color.Yellow);
                return;
            }

            var request = JObject.FromObject(new
            {
                reqId = "LOGIN-TEST",
                messageType = "login",
                accessToken = _accessToken
            }).ToString();

            client.Send(request);

            Console.WriteLine("Request Sent:", ColorTranslator.FromHtml("#00b9f1"));
            Console.WriteLine($"{request}", Color.Green);

            await Task.Delay(_settings.PrintDelay);
        }

        private static async Task GetDetails(IWebSocketClient client, string functionality)
        {
            if (!client.IsRunning)
            {
                Console.WriteLine("Client is not running or connected, please retry!", Color.Yellow);
            }

            var request = JObject.FromObject(new
            {
                reqId = "GET-TEST",
                messageType = functionality.Replace(" ", "").ToLower(CultureInfo.InvariantCulture),
                account = _settings.AccountNumber
            }).ToString();

            client.Send(request);

            Console.WriteLine("Request Sent:", ColorTranslator.FromHtml("#00b9f1"));
            Console.WriteLine($"{request}", Color.Green);

            await Task.Delay(_settings.PrintDelay);
        }

        private static async Task NewOrder(IWebSocketClient client, string orderSide, string orderType, string orderTime)
        {
            if (!client.IsRunning)
            {
                Console.WriteLine("Client is not running or connected, please retry!", Color.Yellow);
            }

            var request =  JObject.FromObject(new
            {
                reqId = "NEW-ORDER-TEST",
                messageType = "neworder",
                account = _settings.AccountNumber,
                clearingAccount = "APIGROUP",
                clOrdId = Guid.NewGuid().ToString(),
                symbol = "C",
                side = orderSide,
                orderQty = 100,
                ordType = orderType,
                price = (orderSide.Equals("BUY", StringComparison.InvariantCultureIgnoreCase)) ? 10.50 : 10.55,
                stopPrice = (orderType.Equals("STOP", StringComparison.InvariantCultureIgnoreCase) || orderType.Equals("STOP_LIMIT", StringComparison.InvariantCultureIgnoreCase))
                                ? orderSide.Equals("BUY", StringComparison.InvariantCultureIgnoreCase)
                                    ? "10.55"
                                    : "10.50"
                                : "",
                exDestination = "INCA",
                subDestination = "NYSE",
                timeInForce = orderTime
            }).ToString();

            client.Send(request);

            Console.WriteLine("Request Sent:", ColorTranslator.FromHtml("#00b9f1"));
            Console.WriteLine($"{request}", Color.Green);

            await Task.Delay(_settings.PrintDelay);
        }

        private static void PromptDisco()
        {
            Console.WriteLine($"Successfully fetched Discovery Document from {_settings.Authority}", Color.Green);
        }

        private static async Task<bool> CheckDisco()
        {
            _cache = new DiscoveryCache($"{_settings.Authority}");
            _disco = await _cache.GetAsync().ConfigureAwait(false);

            if (_disco == null || !string.IsNullOrWhiteSpace(_disco.Error))
            {
                Console.WriteLine("The authority does not respond or responds with an error. Please check the authority " +
                    "address your provided or try again later!", Color.Yellow);
                Console.WriteLine(_disco.Error);

                return false;
            }

            return true;
        }

        private static void PromptCredentials()
        {
            Console.WriteLine("\nPlease provide your credentials.");

            _settings.Username = Prompt.Input<string>("Username", validators: new[] { PromptValidator.Required() });
            _settings.Password = Prompt.Password("Password", validators: new[] { PromptValidator.Required() });
        }

        private static void PromptSettings()
        {
            Console.WriteLine("Settings loaded successfully!", Color.Green);

            Console.Write("Print Delay: ", Color.MediumSlateBlue);
            Console.WriteLine($"{_settings.PrintDelay}ms");

            Console.Write("Authority: ", Color.MediumSlateBlue);
            Console.WriteLine(_settings.Authority);

            Console.Write("Client ID: ", Color.MediumSlateBlue);
            Console.WriteLine(_settings.ClientId);

            Console.Write("Client Secret: ", Color.MediumSlateBlue);
            Console.WriteLine(_settings.ClientSecret);

            Console.Write("Scope(s): ", Color.MediumSlateBlue);
            Console.WriteLine(string.Join(",", _settings.Scope));

            Console.Write("TAPI: ", Color.MediumSlateBlue);
            Console.WriteLine(string.Join(",", _settings.TapiUrl));

            Console.Write("Account Number: ", Color.MediumSlateBlue);
            Console.WriteLine(string.Join(",", _settings.AccountNumber));
        }

        private static void PromptHeader()
        {
            Console.Clear();
            Console.WriteAscii("ALARIC TAPI", Color.MediumSlateBlue);
        }

        private static StyleSheet CreateOptionsStyleSheet()
        {
            var styleSheet = new StyleSheet(Color.White);
            styleSheet.AddStyle(@"\(([^)]+)\)", Color.MediumSlateBlue);

            return styleSheet;
        }

        private static async Task RetrieveToken()
        {
            if (_settings.Username == "token")
            {
                LoadToken();
                return;
            };

            var tokenRequest = new PasswordTokenRequest
            {
                Address = _disco.TokenEndpoint,
                ClientId = _settings.ClientId,
                ClientSecret = _settings.ClientSecret,
                UserName = _settings.Username,
                Password = _settings.Password,
                Scope = string.Join(" ", _settings.Scope)
            };

            tokenRequest.Headers.Add(
                "Authorization",
                $"Basic {Convert.ToBase64String(Encoding.UTF8.GetBytes($"{_settings.ClientId}:{_settings.ClientSecret}"))}");

            var response = await _tokenClient.RequestPasswordTokenAsync(tokenRequest).ConfigureAwait(false);

            if (response.IsError)
            {
                Console.WriteLine("User failed to be authorized! Please (U)pdate your credentials.", Color.Red);
            }
            else
            {
                Console.WriteLine("User successfully authorized!", Color.Green);
            }

            _accessToken = response.AccessToken;
            _refreshToken = response.RefreshToken;
            _expiry = DateTime.Now.AddSeconds(response.ExpiresIn);
        }
        
        private static void LoadToken()
        {
            const string tokenFileName = "token.json";
            Console.WriteLine("Loading token...", Color.Yellow);
            if (!TryGetTokenJson(tokenFileName, out var tokenJson))
            {
                return;
            }

            _accessToken = tokenJson.SelectToken("access_token")?.Value<string>();
            _refreshToken = tokenJson.SelectToken("refresh_token")?.Value<string>();
            if (string.IsNullOrWhiteSpace(_accessToken) || string.IsNullOrWhiteSpace(_refreshToken))
            {
                return;
            }

            if (TryGetExpiryFromToken(_accessToken, out _expiry))
            {
                Console.WriteLine("Token loaded", Color.Yellow);
                
                return;
            }
            
            _refreshToken = string.Empty;
            _accessToken = string.Empty;
        }

        private static bool TryGetTokenJson(string tokenFileName, out JObject tokenJson)
        {
            tokenJson = null;
            if (!File.Exists(tokenFileName))
            {
                Console.WriteLine("token.json is not exist", Color.Red);
                
                return false;
            }
            
            string tokenString;
            try
            {
                tokenString = File.ReadAllText(tokenFileName);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Can not load token from file\n{e}", Color.Red);
                
                return false;
            }

            if (string.IsNullOrWhiteSpace(tokenString))
            {
                Console.WriteLine("Token file is empty", Color.Red);
                
                return false;
            }
            
            try
            {
                tokenJson = JObject.Parse(tokenString);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Can not parse token\n{e}", Color.Red);
                
                return false;
            }

            return true;
        }

        private static bool TryGetExpiryFromToken(string accessToken, out DateTime expiry)
        {
            expiry = default;
            var tokenHandler = new JwtSecurityTokenHandler();
            try
            {
                if (!tokenHandler.CanReadToken(_accessToken))
                {
                    return false;
                }
                
                var jwtToken = tokenHandler.ReadJwtToken(_accessToken);
                _expiry = jwtToken.ValidTo;
                
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine($"Can validate the access token\n{e}", Color.Red);
                
                return false;
            }
        }

        private static async Task RunRefreshTokenAsync(int milliseconds = 30000)
        {
            while (true)
            {
                if (!string.IsNullOrWhiteSpace(_refreshToken)
                    && _expiry.AddMilliseconds(milliseconds * -1) <= DateTime.Now)
                {
                    var response = await RefreshTokenAsync(_refreshToken);

                    if (response.RefreshToken != _refreshToken)
                    {
                        _accessToken = response.AccessToken;
                        _refreshToken = response.RefreshToken;
                        _expiry = DateTime.Now.AddSeconds(response.ExpiresIn);

                        await LoginServer(_client);

                    }
                }

                Task.Delay(milliseconds).Wait();
            }
        }

        private static async Task<TokenResponse> RefreshTokenAsync(string refreshToken)
        {
            return await _tokenClient.RequestRefreshTokenAsync(new RefreshTokenRequest
            {
                Address = _disco.TokenEndpoint,
                ClientId = _settings.ClientId,
                ClientSecret = _settings.ClientSecret,
                RefreshToken = refreshToken
            });
        }

        private static void ValidateToken()
        {
            if (string.IsNullOrWhiteSpace(_accessToken))
            {
                Console.WriteLine("A Token Cannot Be Found. Please, first retrieve a token!", Color.Red);
                return;
            }

            if ((_validator.ValidateAccessTokenAsync(_accessToken)).Result)
            {

                Console.WriteLine("The Token Is Valid!", Color.MediumSlateBlue);
            }
            else
            {
                Console.WriteLine("The Token Is Invalid!", Color.Red);
            }
        }

        private static async Task RetrieveUserInfo()
        {
            var tokenRequest = new UserInfoRequest
            {
                Address = _disco.UserInfoEndpoint,
                ClientId = _settings.ClientId,
                ClientSecret = _settings.ClientSecret,
                Token = _accessToken
            };

            var response = await _tokenClient.GetUserInfoAsync(tokenRequest);

            response.Show();
        }

        private static void CurrentDomainOnProcessExit(object sender, EventArgs eventArgs)
        {
            Console.WriteLine("Exiting process");
            ExitEvent.Set();
        }

        private static void ConsoleOnCancelKeyPress(object sender, ConsoleCancelEventArgs e)
        {
            Console.WriteLine("Canceling process");
            e.Cancel = true;
            ExitEvent.Set();
        }

        private static bool ValidateSettings()
        {
            const string urlPattern = @"http(s)?://([\w-]+.)+[\w-]+(/[\w- ./?%&=])?";
            const string wssPattern = @"wss?://([\w-]+.)+[\w-]+(/[\w- ./?%&=])?";
            const string clientSecretPattern = "[0-9A-Fa-f]{8}[-][0-9A-Fa-f]{4}[-][0-9A-Fa-f]{4}[-][0-9A-Fa-f]{4}[-][0-9A-Fa-f]{12}";

            var settingsTemplate = JToken
                .Parse("{\"PrintDelay\": 3000, \"Authority\": \"https://www.example.com\",\"ClientId\": \"MyClientId\",\"ClientSecret\": \"00000000-0000-0000-0000-000000000000\",\"Scope\": [\"MyScope1\",\"MyScope2\"], \"TapiUrl\": \"wss://tapi-url\", \"AccountNumber\": \"NXXX\"}")
                .ToString(Formatting.Indented);

            if (_settings is null)
            {
                Console.WriteLine("The appsettings.json file cannot be found.", Color.Yellow);
                return false;
            }

            if (string.IsNullOrWhiteSpace(_settings.Authority)
                || string.IsNullOrWhiteSpace(_settings.ClientId)
                || string.IsNullOrWhiteSpace(_settings.ClientSecret)
                || string.IsNullOrWhiteSpace(_settings.TapiUrl)
                || string.IsNullOrWhiteSpace(_settings.AccountNumber)
                || _settings.Scope is null
                || _settings.Scope.Count == 0
                || _settings.Scope.Any(s => string.IsNullOrWhiteSpace(s))
                || _settings.PrintDelay < 0)
            {
                Console.WriteLine("The appsettings.json file is not properly constructed. Ensure that the appsettings.json file complies with the following template", Color.Yellow);
                Console.WriteLine(settingsTemplate, Color.White);

                return false;
            }

            if (!Regex.IsMatch(_settings.Authority, urlPattern))
            {
                Console.WriteLine("The authority URL must be a valid URL, e.g. https://www.example.com", Color.Yellow);

                return false;
            }

            if (!Regex.IsMatch(_settings.TapiUrl, wssPattern))
            {
                Console.WriteLine("The TAPI URL must be a valid URL, e.g. wss://tapi-url", Color.Yellow);

                return false;
            }

            if (!Regex.IsMatch(_settings.ClientSecret, clientSecretPattern))
            {
                Console.WriteLine("The Client Secret must be a valid GUID, e.g. 00000000-0000-0000-0000-000000000000", Color.Yellow);

                return false;
            }

            var logger = LoggerFactory
                .Create(x => x.AddConsole()
                              .AddFilter<DebugLoggerProvider>(null, LogLevel.Trace)
                              .AddFilter<DebugLoggerProvider>(null, LogLevel.Error))
                .CreateLogger<OfflineTokenValidator>();
            _validator = new OfflineTokenValidator(_settings.Authority, _settings.Scope, logger);

            return true;
        }

        private static void LoadSettings()
        {
            _settings = new Settings();
            if (!ConfigurationManager.AppSettings.HasKeys())
                return;

            if (ConfigurationManager.AppSettings.AllKeys.Contains("PrintDelay")) {

                _settings.PrintDelay = 3000;
                if (int.TryParse(ConfigurationManager.AppSettings["PrintDelay"], out var printDelay)) {
                    _settings.PrintDelay = printDelay;
                }
            }
            if (ConfigurationManager.AppSettings.AllKeys.Contains("Authority"))
            {
                _settings.Authority = ConfigurationManager.AppSettings["Authority"];
            }
            if (ConfigurationManager.AppSettings.AllKeys.Contains("ClientId"))
            {
                _settings.ClientId = ConfigurationManager.AppSettings["ClientId"];
            }
            if (ConfigurationManager.AppSettings.AllKeys.Contains("ClientSecret"))
            {
                _settings.ClientSecret = ConfigurationManager.AppSettings["ClientSecret"];
            }
            if (ConfigurationManager.AppSettings.AllKeys.Contains("TapiUrl"))
            {
                _settings.TapiUrl = ConfigurationManager.AppSettings["TapiUrl"];
            }
            if (ConfigurationManager.AppSettings.AllKeys.Contains("Username"))
            {
                _settings.Username = ConfigurationManager.AppSettings["Username"];
            }
            if (ConfigurationManager.AppSettings.AllKeys.Contains("Password"))
            {
                _settings.Password = ConfigurationManager.AppSettings["Password"];
            }
            if (ConfigurationManager.AppSettings.AllKeys.Contains("AccountNumber"))
            {
                _settings.AccountNumber = ConfigurationManager.AppSettings["AccountNumber"];
            }
            if (ConfigurationManager.AppSettings.AllKeys.Contains("IgnoreIdle"))
            {
                _settings.IgnoreIdle = false;
                if (bool.TryParse(ConfigurationManager.AppSettings["IgnoreIdle"], out var ignoreIdle))
                    _settings.IgnoreIdle = ignoreIdle;
            }
            if (ConfigurationManager.AppSettings.AllKeys.Contains("Scope"))
            {
                _settings.Scope = new List<string>();
                var scope = ConfigurationManager.AppSettings["Scope"];
                if (!string.IsNullOrWhiteSpace(scope))
                    _settings.Scope = scope.Split(',').ToList();
            }
        }
    }
}
