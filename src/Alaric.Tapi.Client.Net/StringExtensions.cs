﻿namespace Alaric.Tapi.Client.Net.Validation
{
    public static class StringExtensions
    {
        public static string EnsureLeadingSlash(this string url)
        {
            if (url?.StartsWith("/") == false)
            {
                return "/" + url;
            }

            return url;
        }

        public static string EnsureTrailingSlash(this string url)
        {
            if (url?.EndsWith("/") == false)
            {
                return url + "/";
            }

            return url;
        }

        public static string RemoveLeadingSlash(this string url)
        {
            if (url?.StartsWith("/") == true)
            {
                url = url.Substring(1);
            }

            return url;
        }

        public static string RemoveTrailingSlash(this string url)
        {
            if (url?.EndsWith("/") == true)
            {
                url = url[0..^1];
            }

            return url;
        }

        public static string RemoveScheme(this string url)
        {
            if (url?.StartsWith("http://") == true)
            {
                url = url[7..];
            }
            else if (url?.StartsWith("https://") == true)
            {
                url = url[8..];
            }

            return url;
        }
    }
}
