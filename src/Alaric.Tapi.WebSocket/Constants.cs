﻿namespace Alaric.Tapi.WebSocket
{
    public static class Constants
    {
        internal static class DisconnectReason
        {
            internal const ushort Disposed = 0;
            internal const ushort Lost = 1;
            internal const ushort Idle = 2;
            internal const ushort Error = 3;
            internal const ushort RequestedByUser = 4;
            internal const ushort RequestedByServer = 5;
        }

        internal static class ReconnectReason
        {
            internal const ushort Init = 0;
            internal const ushort Lost = 1;
            internal const ushort Idle = 2;
            internal const ushort Error = 3;
            internal const ushort RequestedByUser = 4;
            internal const ushort RequestedByServer = 5;
        }
    }
}
