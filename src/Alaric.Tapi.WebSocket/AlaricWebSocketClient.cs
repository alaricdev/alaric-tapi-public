﻿using Alaric.Tapi.WebSocket.Helpers;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using Dotnet = System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using static Alaric.Tapi.WebSocket.Constants;
using Alaric.Tapi.WebSocket.Exceptions;
using System.Threading.Channels;
using System.IO;
using System.Linq;

namespace Alaric.Tapi.WebSocket
{
    public class AlaricWebSocketClient : IWebSocketClient
    {
        private const int BufferSize = 1024 * 4;

        private readonly ILogger<AlaricWebSocketClient> _logger;
        private readonly AsyncLock _lock = new AsyncLock();
        private readonly Func<Uri, CancellationToken, Task<Dotnet.WebSocket>> _connectionFactory;
        private readonly Subject<Response> _responseSubject = new Subject<Response>();
        private readonly Subject<ReconnectInfo> _reconnectedSubject = new Subject<ReconnectInfo>();
        private readonly Subject<DisconnectInfo> _disconnectedSubject = new Subject<DisconnectInfo>();
        private readonly Channel<string> _messagesTextToSendQueue = Channel.CreateUnbounded<string>(new UnboundedChannelOptions()
        {
            SingleReader = true,
            SingleWriter = false
        });
        private readonly Channel<byte[]> _messagesBinaryToSendQueue = Channel.CreateUnbounded<byte[]>(new UnboundedChannelOptions()
        {
            SingleReader = true,
            SingleWriter = false
        });

        private Uri _address;
        private Timer _retryTimer;
        private DateTime _lastResponseTime = DateTime.UtcNow;
        private bool _disposing;
        private bool _reconnecting;
        private bool _stopping;
        private bool _isReconnectionEnabled = true;
        private Dotnet.WebSocket _client;
        private CancellationTokenSource _cancellation;
        private CancellationTokenSource _cancellationTotal;

        public AlaricWebSocketClient(Uri address, ILogger<AlaricWebSocketClient> logger, Func<Dotnet.ClientWebSocket> clientFactory = null)
            : this(address, GetClientFactory(clientFactory), logger)
        {
        }

        public AlaricWebSocketClient(Uri address, Func<Uri, CancellationToken, Task<Dotnet.WebSocket>> connectionFactory, ILogger<AlaricWebSocketClient> logger)
        {
            _logger = logger;

            Validations.ValidateInput(address, nameof(address));
            _address = address;

            _connectionFactory = connectionFactory ?? (async (uri, token) =>
            {
                var client = new Dotnet.ClientWebSocket
                {
                    Options = { KeepAliveInterval = new TimeSpan(0, 0, 5, 0) }
                };

                await client.ConnectAsync(uri, token).ConfigureAwait(false);

                return client;
            });
        }

        public Uri Address
        {
            get => _address;
            set
            {
                Validations.ValidateInput(value, nameof(Address));
                _address = value;
            }
        }

        public IObservable<Response> ResponseMessage => _responseSubject.AsObservable();

        public IObservable<ReconnectInfo> Reconnected => _reconnectedSubject.AsObservable();

        public IObservable<DisconnectInfo> Disconnected => _disconnectedSubject.AsObservable();

        public TimeSpan? ReconnectTimeout { get; set; } = TimeSpan.FromMinutes(1);

        public TimeSpan? RetryTimeout { get; set; } = TimeSpan.FromMinutes(1);

        public string Name { get; set; }

        public bool IsStarted { get; private set; }

        public bool IsRunning { get; private set; }

        public bool IsReconnectionEnabled
        {
            get => _isReconnectionEnabled;
            set
            {
                _isReconnectionEnabled = value;

                if (IsStarted)
                {
                    if (_isReconnectionEnabled)
                    {
                        ActivateRetry();
                    }
                    else
                    {
                        DeactivateRetry();
                    }
                }
            }
        }

        public bool IsTextMessageConversionEnabled { get; set; } = true;

        public Dotnet.ClientWebSocket NativeClient => GetSpecificOrThrow(_client);

        public Encoding MessageEncoding { get; set; }

        public void Dispose()
        {
            _disposing = true;
            _logger.LogDebug(LogString("Disposing.."));
            try
            {
                _messagesTextToSendQueue?.Writer.Complete();
                _messagesBinaryToSendQueue?.Writer.Complete();
                _retryTimer?.Dispose();
                _cancellation?.Cancel();
                _cancellationTotal?.Cancel();
                _client?.Abort();
                _client?.Dispose();
                _cancellation?.Dispose();
                _cancellationTotal?.Dispose();
                _responseSubject.OnCompleted();
                _reconnectedSubject.OnCompleted();
            }
            catch (Exception e)
            {
                _logger.LogError(e, LogString($"Failed to dispose client, error: {e.Message}"));
            }

            IsRunning = false;
            IsStarted = false;

            _disconnectedSubject.OnNext(DisconnectInfo.Create(DisconnectReason.Disposed, _client, null));
            _disconnectedSubject.OnCompleted();
        }

        public Task Reconnect()
        {
            return ReconnectInternal(false);
        }

        public Task ReconnectOrFail()
        {
            return ReconnectInternal(true);
        }

        public void Send(string message)
        {
            Validations.ValidateInput(message, nameof(message));

            _messagesTextToSendQueue.Writer.TryWrite(message);
        }

        public void Send(byte[] message)
        {
            Validations.ValidateInput(message, nameof(message));

            _messagesBinaryToSendQueue.Writer.TryWrite(message);
        }

        public Task Start()
        {
            return StartInternal(false);
        }

        public Task StartOrFail()
        {
            return StartInternal(true);
        }

        public async Task<bool> Stop(Dotnet.WebSocketCloseStatus status, string statusDescription)
        {
            var result = await StopInternal(
                client: _client,
                status: status,
                statusDescription: statusDescription,
                cancellation: null,
                failFast: false,
                byServer: false).ConfigureAwait(false);

            _disconnectedSubject.OnNext(DisconnectInfo.Create(DisconnectReason.RequestedByUser, _client, null));

            return result;
        }

        public async Task<bool> StopOrFail(Dotnet.WebSocketCloseStatus status, string statusDescription)
        {
            var result = await StopInternal(
                client: _client,
                status: status,
                statusDescription: statusDescription,
                cancellation: null,
                failFast: true,
                byServer: false).ConfigureAwait(false);

            _disconnectedSubject.OnNext(DisconnectInfo.Create(DisconnectReason.RequestedByUser, _client, null));

            return result;
        }

        private async Task StartInternal(bool failFast)
        {
            if (_disposing)
            {
                throw new WebSocketException(LogString("Client is already disposed, starting not possible"));
            }

            if (IsStarted)
            {
                _logger.LogDebug(LogString("Client already started, ignoring.."));
                return;
            }

            IsStarted = true;

            _logger.LogDebug(LogString("Starting.."));

            _cancellation = new CancellationTokenSource();
            _cancellationTotal = new CancellationTokenSource();

            await StartClient(
                address: _address,
                token: _cancellation.Token,
                reason: ReconnectReason.Init,
                failFast: failFast).ConfigureAwait(false);

            StartBackgroundThreadForSendingText();
            StartBackgroundThreadForSendingBinary();
        }

        private async Task<bool> StopInternal(
            Dotnet.WebSocket client,
            Dotnet.WebSocketCloseStatus status,
            string statusDescription,
            CancellationToken? cancellation,
            bool failFast,
            bool byServer)
        {
            if (_disposing)
            {
                throw new WebSocketException(LogString("Client is already disposed, stopping not possible"));
            }

            var result = false;
            if (client == null)
            {
                IsStarted = false;
                IsRunning = false;
                return false;
            }

            DeactivateRetry();

            try
            {
                var cancellationToken = cancellation ?? CancellationToken.None;
                _stopping = true;
                if (byServer)
                    await client.CloseOutputAsync(status, statusDescription, cancellationToken);
                else
                    await client.CloseAsync(status, statusDescription, cancellationToken);
                result = true;
            }
            catch (Exception e)
            {
                _logger.LogError(e, LogString($"Error while stopping client. Error: '{e.Message}'"));

                if (failFast)
                {
                    throw new WebSocketException($"Failed to stop Websocket client. Error: '{e.Message}'", e);
                }
            }
            finally
            {
                IsStarted = false;
                IsRunning = false;
                _stopping = false;
            }

            return result;
        }

        private static Func<Uri, CancellationToken, Task<Dotnet.WebSocket>> GetClientFactory(Func<Dotnet.ClientWebSocket> clientFactory)
        {
            return clientFactory == null
                ? (Func<Uri, CancellationToken, Task<Dotnet.WebSocket>>)null
                : (async (uri, token) =>
                {
                    var client = clientFactory();
                    await client.ConnectAsync(uri, token).ConfigureAwait(false);
                    return client;
                });
        }

        private void ActivateRetry()
        {
            var retryTimer = (int)RetryTimeout.Value.TotalMilliseconds;
            _retryTimer = new Timer(LastChance, null, retryTimer, retryTimer);
        }

        private void DeactivateRetry()
        {
            _retryTimer?.Dispose();
            _retryTimer = null;
        }

        private void LastChance(object state)
        {
            if (!IsReconnectionEnabled || ReconnectTimeout == null)
            {
                DeactivateRetry();

                return;
            }

            var timeout = Math.Abs(ReconnectTimeout.Value.TotalMilliseconds);
            var difference = Math.Abs(DateTime.UtcNow.Subtract(_lastResponseTime).TotalMilliseconds);

            if (difference > timeout)
            {
                _logger.LogDebug($"Last message received more than {timeout:F}ms ago. Hard restart..");

                DeactivateRetry();

                _ = ReconnectSynchronized(
                    reason: ReconnectReason.Idle,
                    failFast: false,
                    causedException: null);
            }
        }

        private async Task SendTextFromQueue()
        {
            try
            {
                while (await _messagesTextToSendQueue.Reader.WaitToReadAsync())
                {
                    while (_messagesTextToSendQueue.Reader.TryRead(out var message))
                    {
                        try
                        {
                            await SendInternalSynchronized(message).ConfigureAwait(false);
                        }
                        catch (Exception e)
                        {
                            _logger.LogError(e, LogString(
                                $"Failed to send text message: '{message}'. Error: {e.Message}"));
                        }
                    }
                }
            }
            catch (TaskCanceledException e)
            {
                _logger.LogError(e, LogString($"Task was cancelled. Error: {e.Message}"));
            }
            catch (OperationCanceledException e)
            {
                _logger.LogError(e, LogString($"Operation was cancelled. Error: {e.Message}"));
            }
            catch (Exception e)
            {
                if (_cancellationTotal.IsCancellationRequested || _disposing)
                {
                    return;
                }

                _logger.LogTrace(LogString(
                    $"Sending text thread failed. Error: {e.Message}. Creating a new sending thread."));

                StartBackgroundThreadForSendingText();
            }

        }

        private async Task SendBinaryFromQueue()
        {
            try
            {
                while (await _messagesBinaryToSendQueue.Reader.WaitToReadAsync())
                {
                    while (_messagesBinaryToSendQueue.Reader.TryRead(out var message))
                    {
                        try
                        {
                            await SendInternalSynchronized(message).ConfigureAwait(false);
                        }
                        catch (Exception e)
                        {
                            _logger.LogError(e, LogString(
                                $"Failed to send binary message: '{message}'. Error: {e.Message}"));
                        }
                    }
                }
            }
            catch (TaskCanceledException e)
            {
                _logger.LogError(e, LogString($"Task was cancelled. Error: {e.Message}"));
            }
            catch (OperationCanceledException e)
            {
                _logger.LogError(e, LogString($"Operation was cancelled. Error: {e.Message}"));
            }
            catch (Exception e)
            {
                if (_cancellationTotal.IsCancellationRequested || _disposing)
                {
                    return;
                }

                _logger.LogTrace(LogString(
                    $"Sending binary thread failed. Error: {e.Message}. Creating a new sending thread."));

                StartBackgroundThreadForSendingBinary();
            }
        }

        private void StartBackgroundThreadForSendingText()
        {
            _ = Task.Factory.StartNew(
                function: _ => SendTextFromQueue(),
                state: TaskCreationOptions.LongRunning,
                cancellationToken: _cancellationTotal.Token);
        }

        private void StartBackgroundThreadForSendingBinary()
        {
            _ = Task.Factory.StartNew(
                function: _ => SendBinaryFromQueue(),
                state: TaskCreationOptions.LongRunning,
                cancellationToken: _cancellationTotal.Token);
        }

        private async Task SendInternalSynchronized(string message)
        {
            using (await _lock.LockAsync())
            {
                await SendInternal(message);
            }
        }

        private async Task SendInternal(string message)
        {
            if (!IsClientConnected())
            {
                _logger.LogDebug(LogString($"Client is not connected to server, cannot send:  {message}"));

                return;
            }

            _logger.LogTrace(LogString($"Sending:  {message}"));

            var buffer = GetEncoding().GetBytes(message);
            var messageSegment = new ArraySegment<byte>(buffer);

            await _client
                .SendAsync(
                    buffer: messageSegment,
                    messageType: Dotnet.WebSocketMessageType.Text,
                    endOfMessage: true,
                    cancellationToken: _cancellation.Token)
                .ConfigureAwait(false);
        }

        private async Task SendInternalSynchronized(byte[] message)
        {
            using (await _lock.LockAsync())
            {
                await SendInternal(message);
            }
        }

        private async Task SendInternal(byte[] message)
        {
            if (!IsClientConnected())
            {
                _logger.LogDebug(LogString(
                    $"Client is not connected to server, cannot send binary, length: {message.Length}"));

                return;
            }

            _logger.LogTrace(LogString($"Sending binary, length: {message.Length}"));

            await _client
                .SendAsync(
                    buffer: new ArraySegment<byte>(message),
                    messageType: Dotnet.WebSocketMessageType.Binary,
                    endOfMessage: true,
                    cancellationToken: _cancellation.Token).ConfigureAwait(false);
        }

        private async Task ReconnectInternal(bool failFast)
        {
            if (!IsStarted)
            {
                _logger.LogDebug(LogString("Client not started, ignoring reconnection.."));
                return;
            }

            try
            {
                await ReconnectSynchronized(
                    reason: ReconnectReason.RequestedByUser,
                    failFast: failFast,
                    causedException: null).ConfigureAwait(false);
            }
            finally
            {
                _reconnecting = false;
            }
        }

        private async Task ReconnectSynchronized(ushort reason, bool failFast, Exception causedException)
        {
            using (await _lock.LockAsync())
            {
                await Reconnect(reason, failFast, causedException);
            }
        }

        private async Task Reconnect(ushort reason, bool failFast, Exception exception)
        {
            IsRunning = false;

            if (_disposing)
            {
                return;
            }

            _reconnecting = true;

            var info = DisconnectInfo.Create(reason, _client, exception);

            if (reason != DisconnectReason.Error)
            {
                _disconnectedSubject.OnNext(info);

                if (info.CancelReconnect)
                {
                    _logger.LogInformation(LogString("Reconnecting canceled by user, exiting."));
                }
            }

            _cancellation.Cancel();

            _client?.Abort();
            _client?.Dispose();

            if (!IsReconnectionEnabled || info.CancelReconnect)
            {
                IsStarted = false;
                _reconnecting = false;

                return;
            }

            _logger.LogDebug(LogString("Reconnecting.."));

            _cancellation = new CancellationTokenSource();

            await StartClient(_address, _cancellation.Token, reason, failFast).ConfigureAwait(false);
            _reconnecting = false;
        }

        private async Task StartClient(Uri address, CancellationToken token, ushort reason, bool failFast)
        {
            DeactivateRetry();

            try
            {
                _client = await _connectionFactory(address, token).ConfigureAwait(false);

                IsRunning = true;
                IsStarted = true;

                _reconnectedSubject.OnNext(ReconnectInfo.Create(reason));

                _ = Listen(_client, token);

                _lastResponseTime = DateTime.UtcNow;
                ActivateRetry();
            }
            catch (Exception e)
            {
                var info = DisconnectInfo.Create(DisconnectReason.Error, _client, e);
                _disconnectedSubject.OnNext(info);

                if (info.CancelReconnect)
                {
                    _logger.LogError(e, LogString("Exception while connecting. " +
                        $"Reconnecting canceled by user, exiting. Error: {e.Message}"));
                    return;
                }

                if (failFast)
                {
                    throw new WebSocketException($"Failed to start Websocket client, error: '{e.Message}'", e);
                }

                if (RetryTimeout == null)
                {
                    _logger.LogError(e, LogString("Exception while connecting. " +
                        $"Reconnecting is disabled, exiting. Error: {e.Message}"));
                    return;
                }

                var timeout = RetryTimeout.Value;

                _logger.LogError(e, LogString("Exception while connecting. " +
                        $"Waiting {timeout.TotalSeconds} seconds before next reconnection try. Error: '{e.Message}'"));

                await Task.Delay(timeout, token).ConfigureAwait(false);
                await Reconnect(
                    reason: ReconnectReason.Error,
                    failFast: false,
                    exception: e).ConfigureAwait(false);
            }
        }

        private async Task Listen(Dotnet.WebSocket client, CancellationToken token)
        {
            Exception exception = null;

            try
            {
                var buffer = new ArraySegment<byte>(new byte[BufferSize]);

                do
                {
                    Dotnet.WebSocketReceiveResult result;
                    byte[] resultArrayWithTrailing = null;
                    var resultArraySize = 0;
                    var isResultArrayCloned = false;
                    MemoryStream memoryStream = null;

                    while (true)
                    {
                        result = await client.ReceiveAsync(buffer, token);

                        var currentChunk = buffer.Array;
                        var currentChunkSize = result.Count;
                        var isFirstChunk = resultArrayWithTrailing == null;

                        if (isFirstChunk)
                        {
                            resultArraySize += currentChunkSize;
                            resultArrayWithTrailing = currentChunk;
                            isResultArrayCloned = false;
                        }
                        else if (currentChunk != null)
                        {
                            if (memoryStream == null)
                            {
                                memoryStream = new MemoryStream();
                                memoryStream.Write(resultArrayWithTrailing, 0, resultArraySize);
                            }

                            memoryStream.Write(currentChunk, buffer.Offset, currentChunkSize);
                        }

                        if (result.EndOfMessage)
                        {
                            break;
                        }

                        if (isResultArrayCloned)
                        {
                            continue;
                        }

                        resultArrayWithTrailing = resultArrayWithTrailing?.ToArray();
                        isResultArrayCloned = true;
                    }

                    memoryStream?.Seek(0, SeekOrigin.Begin);

                    Response message;
                    if (result.MessageType == Dotnet.WebSocketMessageType.Text && IsTextMessageConversionEnabled)
                    {
                        var data = memoryStream != null
                            ? GetEncoding().GetString(memoryStream.ToArray())
                            : resultArrayWithTrailing != null
                                ? GetEncoding().GetString(resultArrayWithTrailing, 0, resultArraySize)
                                : null;

                        message = Response.TextMessage(data);
                    }
                    else if (result.MessageType == Dotnet.WebSocketMessageType.Close)
                    {
                        _logger.LogTrace(LogString("Received Close message.."));

                        var info = DisconnectInfo.Create(DisconnectReason.RequestedByServer, client, null);
                        _disconnectedSubject.OnNext(info);

                        if (info.CancelDisconnect)
                        {
                            if (IsReconnectionEnabled)
                            {
                                throw new OperationCanceledException("Websocket connection was closed by the server!");
                            }

                            continue;
                        }

                        await StopInternal(
                            client: client,
                            status: Dotnet.WebSocketCloseStatus.NormalClosure,
                            statusDescription: "Closing",
                            cancellation: token,
                            failFast: false,
                            byServer: true);

                        if (IsReconnectionEnabled && !ShouldIgnoreReconnection(client))
                        {
                            _ = ReconnectSynchronized(
                                reason: ReconnectReason.Lost,
                                failFast: false,
                                causedException: null);
                        }

                        return;
                    }
                    else
                    {
                        if (memoryStream != null)
                        {
                            message = Response.BinaryMessage(memoryStream.ToArray());
                        }
                        else
                        {
                            Array.Resize(ref resultArrayWithTrailing, resultArraySize);
                            message = Response.BinaryMessage(resultArrayWithTrailing);
                        }
                    }

                    memoryStream?.Dispose();

                    //_logger.LogTrace(LogString($"Received:  {message}"));

                    _lastResponseTime = DateTime.UtcNow;
                    _responseSubject.OnNext(message);

                } while (client.State == Dotnet.WebSocketState.Open && !token.IsCancellationRequested);
            }
            catch (TaskCanceledException e)
            {
                exception = e;
            }
            catch (OperationCanceledException e)
            {
                exception = e;
            }
            catch (ObjectDisposedException e)
            {
                exception = e;
            }
            catch (Exception e)
            {
                _logger.LogError(e, LogString($"Error while listening to websocket stream, error: '{e.Message}'"));
                exception = e;
            }

            if (ShouldIgnoreReconnection(client) || !IsStarted)
            {
                return;
            }

            _ = ReconnectSynchronized(
                reason: ReconnectReason.Lost,
                failFast: false,
                causedException: exception);
        }

        private bool IsClientConnected()
        {
            return _client.State == Dotnet.WebSocketState.Open;
        }

        private bool ShouldIgnoreReconnection(Dotnet.WebSocket client)
        {
            return ReconnectionIsInProgress()
                || AlreadyConnectedWithDifferentClient(client);
        }

        private bool ReconnectionIsInProgress()
        {
            return _disposing || _reconnecting || _stopping;
        }

        private bool AlreadyConnectedWithDifferentClient(Dotnet.WebSocket client)
        {
            return client != _client;
        }

        private Encoding GetEncoding()
        {
            if (MessageEncoding == null)
            {
                MessageEncoding = Encoding.UTF8;
            }

            return MessageEncoding;
        }

        private Dotnet.ClientWebSocket GetSpecificOrThrow(Dotnet.WebSocket client)
        {
            if (client == null)
            {
                return null;
            }

            if (!(client is Dotnet.ClientWebSocket specific))
            {
                throw new WebSocketException(
                    "Cannot cast [WebSocket] client to [ClientWebSocket], " +
                    "please provide a correct type using the factory");
            }

            return specific;
        }

        private string LogString(string msg)
        {
            return $"[WEBSOCKET {Name ?? "CLIENT"}] {msg}";
        }
    }
}
