﻿using System;

namespace Alaric.Tapi.WebSocket.Exceptions
{
    public class InputException : WebSocketException
    {
        public InputException()
        {
        }

        public InputException(string message)
            : base(message)
        {
        }

        public InputException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
