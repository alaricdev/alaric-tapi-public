﻿namespace Alaric.Tapi.WebSocket
{
    public class ReconnectInfo
    {
        public ReconnectInfo(ushort reason)
        {
            Reason = reason;
        }

        public ushort Reason { get; }

        public static ReconnectInfo Create(ushort reason)
        {
            return new ReconnectInfo(reason);
        }
    }
}
