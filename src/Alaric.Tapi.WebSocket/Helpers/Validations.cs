﻿using System.Collections.Generic;
using System.Linq;
using Alaric.Tapi.WebSocket.Exceptions;

namespace Alaric.Tapi.WebSocket.Helpers
{
    internal static class Validations
    {
        public static void ValidateInput(string value, string name)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new InputException($"Input string parameter '{name}' is null or empty. Please correct it.");
            }
        }

        public static void ValidateInput<T>(T value, string name)
        {
            if (Equals(value, default(T)))
            {
                throw new InputException($"Input parameter '{name}' is null. Please correct it.");
            }
        }

        public static void ValidateInputCollection<T>(IEnumerable<T> collection, string name)
        {
            ValidateInput(collection, name);

            if (!collection.Any())
            {
                throw new InputException($"Input collection '{name}' is empty. Please correct it.");
            }
        }

        public static void ValidateInput(int value, string name, int minValue = int.MinValue, int maxValue = int.MaxValue)
        {
            if (value < minValue)
            {
                throw new InputException($"Input parameter '{name}' is lower than {minValue}. Please correct it.");
            }
            if (value > maxValue)
            {
                throw new InputException($"Input parameter '{name}' is higher than {maxValue}. Please correct it.");
            }
        }

        public static void ValidateInput(long value, string name, long minValue = long.MinValue, long maxValue = long.MaxValue)
        {
            if (value < minValue)
            {
                throw new InputException($"Input parameter '{name}' is lower than {minValue}. Please correct it.");
            }
            if (value > maxValue)
            {
                throw new InputException($"Input parameter '{name}' is higher than {maxValue}. Please correct it.");
            }
        }

        public static void ValidateInput(double value, string name, double minValue = double.MinValue, double maxValue = double.MaxValue)
        {
            if (value < minValue)
            {
                throw new InputException($"Input parameter '{name}' is lower than {minValue}. Please correct it.");
            }
            if (value > maxValue)
            {
                throw new InputException($"Input parameter '{name}' is higher than {maxValue}. Please correct it.");
            }
        }
    }
}
