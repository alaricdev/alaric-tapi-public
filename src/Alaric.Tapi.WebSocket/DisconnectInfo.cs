﻿using System;
using System.Net.WebSockets;

namespace Alaric.Tapi.WebSocket
{
    public class DisconnectInfo
    {
        public DisconnectInfo(
            ushort reason,
            WebSocketCloseStatus? status,
            string description,
            string subProtocol,
            Exception exception)
        {
            Reason = reason;
            Status = status;
            Description = description;
            SubProtocol = subProtocol;
            Exception = exception;
        }

        public ushort Reason { get; }

        public WebSocketCloseStatus? Status { get; }

        public string Description { get; }

        public string SubProtocol { get; }

        public Exception Exception { get; }

        public bool CancelReconnect { get; set; }

        public bool CancelDisconnect { get; set; }

        public static DisconnectInfo Create(
            ushort reason,
            System.Net.WebSockets.WebSocket client,
            Exception exception)
        {
            return new DisconnectInfo(
                reason,
                client?.CloseStatus,
                client?.CloseStatusDescription,
                client?.SubProtocol,
                exception);
        }
    }
}
