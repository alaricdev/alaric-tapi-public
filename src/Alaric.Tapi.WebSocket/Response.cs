﻿using System.Net.WebSockets;

namespace Alaric.Tapi.WebSocket
{
    public sealed class Response
    {
        private Response(byte[] binary, string text, WebSocketMessageType messageType)
        {
            Binary = binary;
            Text = text;
            MessageType = messageType;
        }

        public string Text { get; }

        public byte[] Binary { get; }

        public WebSocketMessageType MessageType { get; }

        public override string ToString()
        {
            if (MessageType == WebSocketMessageType.Text)
            {
                return Text;
            }

            return $"Type binary, length: {Binary?.Length}";
        }

        public static Response TextMessage(string data)
        {
            return new Response(null, data, WebSocketMessageType.Text);
        }

        public static Response BinaryMessage(byte[] data)
        {
            return new Response(data, null, WebSocketMessageType.Binary);
        }
    }
}