﻿using System;
using System.Net.WebSockets;
using System.Text;
using System.Threading.Tasks;

namespace Alaric.Tapi.WebSocket
{
    public interface IWebSocketClient : IDisposable
    {
        Uri Address { get; set; }

        IObservable<Response> ResponseMessage { get; }

        IObservable<ReconnectInfo> Reconnected { get; }

        IObservable<DisconnectInfo> Disconnected { get; }

        TimeSpan? ReconnectTimeout { get; set; }

        TimeSpan? RetryTimeout { get; set; }

        string Name { get; set; }

        bool IsStarted { get; }

        bool IsRunning { get; }

        bool IsReconnectionEnabled { get; set; }

        bool IsTextMessageConversionEnabled { get; set; }

        ClientWebSocket NativeClient { get; }

        Encoding MessageEncoding { get; set; }

        Task Start();

        Task StartOrFail();

        Task<bool> Stop(WebSocketCloseStatus status, string statusDescription);

        Task<bool> StopOrFail(WebSocketCloseStatus status, string statusDescription);

        Task Reconnect();

        Task ReconnectOrFail();

        void Send(string message);

        void Send(byte[] message);
    }
}
