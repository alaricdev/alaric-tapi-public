﻿using IdentityModel;
using IdentityModel.Client;
using Newtonsoft.Json.Linq;
using System;
using System.Drawing;
using System.Text;
using Console = Colorful.Console;

namespace Alaric.Tapi.Client
{
    public static class TokenExtensions
    {
        public static void Show(this UserInfoResponse response)
        {
            if (!response.IsError)
            {
                Console.WriteLine("\n========== User Info Response ==========", Color.Yellow);

                Console.WriteLine(response.Json.ToString(), ColorTranslator.FromHtml("#d63aff"));
            }
            else
            {
                if (response.ErrorType == ResponseErrorType.Http)
                {
                    Console.Write("HTTP Error: ", Color.Red);
                    Console.WriteLine(response.Error, Color.Red);
                    Console.Write("HTTP Status Code: ", Color.Red);
                    Console.WriteLine(response.HttpStatusCode, Color.Red);
                }
                else
                {
                    Console.Write("Protocol Error: ", Color.Red);
                    Console.WriteLine(response.Raw, Color.Red);
                }
            }
        }

        public static void Show(this TokenResponse response)
        {
            if (!response.IsError)
            {
                Console.WriteLine("\n========== Token Response ==========", Color.Yellow);

                if (response.AccessToken.Contains("."))
                {
                    var parts = response.AccessToken.Split('.');
                    var header = parts[0];
                    var payload = parts[1];
                    var signature = parts[2];

                    Console.Write(header, ColorTranslator.FromHtml("#fb015b"));
                    Console.Write(".", Color.White);
                    Console.Write(payload, ColorTranslator.FromHtml("#d63aff"));
                    Console.Write(".", Color.White);
                    Console.Write(signature, ColorTranslator.FromHtml("#00b9f1"));

                    Console.WriteLine("\n\n========== Token Response - Decoded ==========", Color.Yellow);

                    Console.WriteLine(JObject.Parse(Encoding.UTF8.GetString(Base64Url.Decode(header))), ColorTranslator.FromHtml("#fb015b"));
                    Console.WriteLine(JObject.Parse(Encoding.UTF8.GetString(Base64Url.Decode(payload))), ColorTranslator.FromHtml("#d63aff"));
                }
                else
                {
                    Console.Write("Invalid Token: ", Color.Red);
                    Console.WriteLine(response.Json, Color.Red);
                }
            }
            else
            {
                if (response.ErrorType == ResponseErrorType.Http)
                {
                    Console.Write("HTTP Error: ", Color.Red);
                    Console.WriteLine(response.Error, Color.Red);
                    Console.Write("HTTP Status Code: ", Color.Red);
                    Console.WriteLine(response.HttpStatusCode, Color.Red);
                }
                else
                {
                    Console.Write("Protocol Error: ", Color.Red);
                    Console.WriteLine(response.Raw, Color.Red);
                }
            }
        }
    }
}
