﻿using System.Collections.Generic;

namespace Alaric.Tapi
{
    public class Settings
    {
        public int PrintDelay { get; set; }

        public string Authority { get; set; }

        public string ClientId { get; set; }

        public string ClientSecret { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public List<string> Scope { get; set; }

        public string TapiUrl { get; set; }

        public string AccountNumber { get; set; }

        public bool IgnoreIdle { get; set; }
    }
}
