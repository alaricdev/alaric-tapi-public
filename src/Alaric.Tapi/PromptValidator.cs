﻿using System;
using System.Text.RegularExpressions;
using Sharprompt;

namespace Alaric.Tapi
{
    public static class PromptValidator
    {
        public static Func<object, ValidationError> Required()
        {
            return input =>
            {
                if (input is string strValue && !string.IsNullOrEmpty(strValue))
                {
                    return null;
                }

                return new ValidationError("Value is required!");
            };
        }

        public static Func<object, ValidationError> MinLength(int length)
        {
            return input =>
            {
                if (!(input is string strValue))
                {
                    return null;
                }

                if (strValue.Length >= length)
                {
                    return null;
                }

                return new ValidationError($"Value must be longer than or equal to {length} characters!");
            };
        }

        public static Func<object, ValidationError> MaxLength(int length)
        {
            return input =>
            {
                if (!(input is string strValue))
                {
                    return null;
                }

                if (strValue.Length <= length)
                {
                    return null;
                }

                if (length == 1)
                {
                    return new ValidationError("Value must be a single valid option!");
                }
                return new ValidationError($"Value must be shorter than or equal to {length} characters!");
            };
        }

        public static Func<object, ValidationError> RegularExpression(string pattern)
        {
            return input =>
            {
                if (!(input is string strValue))
                {
                    return null;
                }

                if (Regex.IsMatch(strValue, pattern))
                {
                    return null;
                }

                return new ValidationError("Please select a valid option!");
            };
        }
    }
}
