﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Alaric.Tapi.Validation
{
    public class JsonWebKeySet
    {
        public JsonWebKeySet()
        {
        }

        public JsonWebKeySet(string json)
        {
            if (string.IsNullOrWhiteSpace(json))
            {
                throw new ArgumentNullException(nameof(json));
            }

            try
            {
                SerializeJson(json);
            }
            catch (JsonSerializationException ex)
            {
                throw new ArgumentException("JWKS JSON failed to be serialized!", ex);
            }
        }

        private void SerializeJson(string json)
        {
            var errors = new List<string>();

            var jwks = JsonConvert.DeserializeObject<JsonWebKeySet>(
                json,
                new JsonSerializerSettings
                {
                    Error = (object sender, ErrorEventArgs args) =>
                    {
                        errors.Add(args.ErrorContext.Error.Message);
                        args.ErrorContext.Handled = true;
                    }
                });

            if (errors.Count > 0)
            {
                throw new JsonSerializationException(string.Join("|", errors));
            }

            Keys = jwks.Keys;
        }

        [JsonProperty(
            DefaultValueHandling = DefaultValueHandling.Ignore,
            NullValueHandling = NullValueHandling.Ignore,
            PropertyName = "keys",
            Required = Required.Default)]
        public ICollection<JsonWebKey> Keys { get; set; }
    }
}
