﻿using System;
using System.Threading.Tasks;

namespace Alaric.Tapi.Validation
{
    public interface ICache<TData> where TData : class
    {
        Task<TData> GetAsync();

        Task<TData> SetAsync(TimeSpan expiration);
    }
}
