﻿using System;
using IdentityModel.Client;

namespace Alaric.Tapi.Validation
{
    public interface IOfflineDiscoveryCache : IDiscoveryCache
    {
        TimeSpan CacheResetDays { get; set; }

        CacheResetPolicy CacheResetPolicy { get; set; }
    }

    public enum CacheResetPolicy
    {
        None,
        Day,
        Duration
    }
}
