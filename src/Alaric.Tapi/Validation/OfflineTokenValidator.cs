﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;

namespace Alaric.Tapi.Validation
{
    public class OfflineTokenValidator : IOfflineTokenValidator
    {
        private readonly ILogger<OfflineTokenValidator> _logger;
        private readonly OfflineDiscoveryCache _cache;
        private readonly string _authority;
        private readonly IList<string> _audiences;
        private readonly string[] _publicKeys;

        public OfflineTokenValidator(string authority, IList<string> audience, ILogger<OfflineTokenValidator> logger)
        {
            _authority = authority ?? throw new ArgumentNullException(nameof(authority));
            _audiences = audience ?? throw new ArgumentNullException(nameof(audience));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _cache = new OfflineDiscoveryCache(authority);
            _publicKeys = new string[2];

            IdentityModelEventSource.ShowPII = true;
        }

        public async Task<bool> ValidateAccessTokenAsync(string token)
        {
            var offlineDisco = await _cache.GetAsync();

            if (offlineDisco == null || !string.IsNullOrWhiteSpace(offlineDisco.Error))
            {
                _logger.LogError(offlineDisco.Error, "The authority does not respond or responds with an error. " +
                    "Please check the authority address your provided or try again later!");

                return false;
            }

            var keyset = offlineDisco.KeySet;

            if (keyset == null
                || keyset.Keys == null
                || keyset.Keys.Count == 0
                || keyset.Keys[0].X5c == null
                || keyset.Keys[0].X5c.Count == 0
                || string.IsNullOrWhiteSpace(keyset.Keys[0].X5c[0]))
            {
                _logger.LogError("Cannot Fetch the Public Key!");

                return false;
            }

            var publicKey = keyset.Keys[0].X5c[0];
            if (!string.Equals(_publicKeys[1] ?? string.Empty, publicKey, StringComparison.OrdinalIgnoreCase))
            {
                _publicKeys[0] = _publicKeys[1] ?? string.Empty;
                _publicKeys[1] = publicKey;
            }

            return ValidateAccessToken(token, _publicKeys[0])
                    || ValidateAccessToken(token, _publicKeys[1]);
        }

        private bool ValidateAccessToken(string token, string publicKey)
        {
            if (string.IsNullOrWhiteSpace(publicKey))
            {
                _logger.LogWarning("The Public Key Is Invalid!");

                return false;
            }

            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = true,
                AudienceValidator = ValidateAudiences,
                ValidateIssuer = true,
                ValidIssuer = _authority,
                ValidateActor = false,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new X509SecurityKey(new X509Certificate2(Convert.FromBase64String(publicKey)))
            };

            var tokenHandler = new JwtSecurityTokenHandler();

            try
            {
                var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out var rawValidatedToken);

                return rawValidatedToken is JwtSecurityToken securityToken;
            }
            catch (Exception ex)
            {
                _logger.LogWarning(ex, "The Token Is Invalid!");

                return false;
            }
        }

        private bool ValidateAudiences(
            IEnumerable<string> audiences,
            SecurityToken securityToken,
            TokenValidationParameters validationParameters)
        {
            return audiences.Any(audience => _audiences.Contains(audience, StringComparer.OrdinalIgnoreCase));
        }
    }
}
