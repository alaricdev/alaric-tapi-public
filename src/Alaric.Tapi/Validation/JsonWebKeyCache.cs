﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;

namespace Alaric.Tapi.Validation
{
    public class JsonWebKeyCache : ICache<JsonWebKey>
    {
        private readonly IMemoryCache _cache;
        private readonly string _authority;
        private readonly ILogger _logger;

        public JsonWebKeyCache(string authority, ILogger logger)
        {
            _authority = authority ?? throw new ArgumentNullException(nameof(authority));
            _cache = new MemoryCache(new MemoryCacheOptions());
            _logger = logger;
        }

        public JsonWebKeyCache(string authority, ILogger logger, IMemoryCache cache)
        {
            _authority = authority ?? throw new ArgumentNullException(nameof(authority));
            _cache = cache ?? new MemoryCache(new MemoryCacheOptions());
            _logger = logger;
        }

        public async Task<JsonWebKey> GetAsync()
        {
            if (!_cache.TryGetValue<JsonWebKey>(CacheKeys.Jwk, out var jwk))
            {
                jwk = await SetAsync(KeyDefaults.DefaultCacheDuration);
            }

            return jwk;
        }

        public async Task<JsonWebKey> SetAsync(TimeSpan expiration)
        {
            var jwks = await FetchJwksAsync();

            var signingKey = jwks.Keys.FirstOrDefault(k => k.Usage == KeyDefaults.UsageAsSigningKey);

            _cache.Set(CacheKeys.Jwk, signingKey, expiration);

            return signingKey;
        }

        private async Task<JsonWebKeySet> FetchJwksAsync()
        {
            var jwksEndpoint = new UriBuilder
            {
                Scheme = "https",
                Host = _authority.RemoveScheme().RemoveTrailingSlash(),
                Path = ProtocolRoutePaths.DiscoveryWebKeys
            };

            using var htppClient = new HttpClient();

            try
            {
                var response = await htppClient.GetAsync(jwksEndpoint.Uri, HttpCompletionOption.ResponseContentRead);

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();

                    return new JsonWebKeySet(content);
                }
            }
            catch (ArgumentNullException ex)
            {
                _logger?.LogError(ex, "An argument is null, please refer to your Administrator.", nameof(FetchJwksAsync));
            }
            catch (HttpRequestException ex)
            {
                _logger?.LogError(ex, "A request error was thrown, please refer to your Administrator.", nameof(FetchJwksAsync));
            }
            catch (Exception ex)
            {
                _logger?.LogError(ex, "An unknown error was thrown, please refer to your Administrator.", nameof(FetchJwksAsync));
                throw;
            }

            return null;
        }
    }
}
