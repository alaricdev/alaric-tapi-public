﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using IdentityModel.Client;

namespace Alaric.Tapi.Validation
{
    public class OfflineDiscoveryCache : IOfflineDiscoveryCache
    {
        private DateTime _nextReload = DateTime.MinValue;
        private AsyncLazy<DiscoveryDocumentResponse> _lazyResponse;

        private readonly DiscoveryPolicy _policy;
        private readonly Func<HttpMessageInvoker> _getHttpClient;
        private readonly string _authority;

        public OfflineDiscoveryCache(string authority, DiscoveryPolicy policy = null)
        {
            _authority = authority;
            _policy = policy ?? new DiscoveryPolicy();
            _getHttpClient = () => new HttpClient();
        }

        public OfflineDiscoveryCache(string authority, Func<HttpMessageInvoker> httpClientFunc, DiscoveryPolicy policy = null)
        {
            _authority = authority;
            _policy = policy ?? new DiscoveryPolicy();
            _getHttpClient = httpClientFunc ?? throw new ArgumentNullException(nameof(httpClientFunc));
        }

        public TimeSpan CacheDuration { get; set; } = TimeSpan.FromHours(24);

        public TimeSpan CacheResetDays { get; set; } = TimeSpan.FromDays(1);

        public CacheResetPolicy CacheResetPolicy { get; set; } = CacheResetPolicy.None;

        public Task<DiscoveryDocumentResponse> GetAsync()
        {
            if (_nextReload <= DateTime.UtcNow)
            {
                Refresh();
            }

            return _lazyResponse.Value;
        }

        public void Refresh()
        {
            _lazyResponse = new AsyncLazy<DiscoveryDocumentResponse>(GetOfflineResponseAsync);
        }

        private async Task<DiscoveryDocumentResponse> GetOfflineResponseAsync()
        {
            var result = await _getHttpClient().GetDiscoveryDocumentAsync(new DiscoveryDocumentRequest
            {
                Address = _authority,
                Policy = _policy
            }).ConfigureAwait(false);

            if (result.IsError)
            {
                Refresh();

                _nextReload = DateTime.MinValue;
            }
            else
            {
                switch (CacheResetPolicy)
                {
                    case CacheResetPolicy.Day:
                    case CacheResetPolicy.None:
                        _nextReload = DateTime.Today.AddSeconds(CacheResetDays.TotalSeconds - 1);
                        break;
                    case CacheResetPolicy.Duration:
                        _nextReload = DateTime.UtcNow.Add(CacheDuration);
                        break;
                    default:
                        _nextReload = DateTime.Today.AddSeconds(CacheResetDays.TotalSeconds - 1);
                        break;
                }

            }

            return result;
        }
    }
}
