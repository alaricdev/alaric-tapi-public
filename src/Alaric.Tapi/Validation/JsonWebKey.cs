﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Alaric.Tapi.Validation
{
    public class JsonWebKey
    {
        public JsonWebKey()
        {
        }

        public JsonWebKey(string json)
        {
            if (string.IsNullOrWhiteSpace(json))
            {
                throw new ArgumentNullException(nameof(json));
            }

            try
            {
                BuildJsonWebKey(SerializeJson(json));
            }
            catch (JsonSerializationException ex)
            {
                throw new ArgumentException("JWK JSON failed to be serialized!", ex);
            }
        }

        private static JsonWebKey SerializeJson(string json)
        {
            var errors = new List<string>();

            var jwk = JsonConvert.DeserializeObject<JsonWebKey>(
                json,
                new JsonSerializerSettings
                {
                    Error = (object sender, ErrorEventArgs args) =>
                    {
                        errors.Add(args.ErrorContext.Error.Message);
                        args.ErrorContext.Handled = true;
                    }
                });

            if (errors.Count > 0)
            {
                throw new JsonSerializationException(string.Join("|", errors));
            }

            return jwk;
        }

        private void BuildJsonWebKey(JsonWebKey jsonWebKey)
        {
            Algorithm = jsonWebKey.Algorithm;
            KeyType = jsonWebKey.KeyType;
            Usage = jsonWebKey.Usage;
            PublicKey = jsonWebKey.PublicKeys.FirstOrDefault();
            Exponent = jsonWebKey.Exponent;
            Moduluos = jsonWebKey.Moduluos;
            UniqueIdentifier = jsonWebKey.UniqueIdentifier;
            Thumbprint = jsonWebKey.Thumbprint;
        }

        /// <summary>
        /// The algorithm for the key, e.g. RS256. Mapped from <c>alg</c>.
        /// </summary>
        [JsonProperty(
            DefaultValueHandling = DefaultValueHandling.Ignore,
            NullValueHandling = NullValueHandling.Ignore,
            PropertyName = "alg",
            Required = Required.Default)]
        public string Algorithm { get; set; }

        /// <summary>
        /// The key type, e.g. RSA. Mapped from <c>kty</c>.
        /// </summary>
        [JsonProperty(
            DefaultValueHandling = DefaultValueHandling.Ignore,
            NullValueHandling = NullValueHandling.Ignore,
            PropertyName = "kty",
            Required = Required.Default)]
        public string KeyType { get; set; }

        /// <summary>
        /// Defines how the key was meant to be used, e.g. <c>sig/c> for signing key. Mapped from <c>use</c>.
        /// </summary>
        [JsonProperty(
            DefaultValueHandling = DefaultValueHandling.Ignore,
            NullValueHandling = NullValueHandling.Ignore,
            PropertyName = "use",
            Required = Required.Default)]
        public string Usage { get; set; }

        /// <summary>
        /// The public key or the x.509 certificate chain. Mapped from <c>x5c</c>.
        /// </summary>
        [JsonProperty(
            DefaultValueHandling = DefaultValueHandling.Ignore,
            NullValueHandling = NullValueHandling.Ignore,
            PropertyName = "x5c",
            Required = Required.Default)]
        public ICollection<string> PublicKeys { get; set; }

        [JsonIgnore]
        public string PublicKey { get; set; }

        /// <summary>
        /// The exponent for a standard PEM. Mapped from <c>e</c>.
        /// </summary>
        [JsonProperty(
            DefaultValueHandling = DefaultValueHandling.Ignore,
            NullValueHandling = NullValueHandling.Ignore,
            PropertyName = "e",
            Required = Required.Default)]
        public string Exponent { get; set; }

        /// <summary>
        /// The moduluos for a standard PEM. Mapped from <c>n</c>.
        /// </summary>
        [JsonProperty(
            DefaultValueHandling = DefaultValueHandling.Ignore,
            NullValueHandling = NullValueHandling.Ignore,
            PropertyName = "n",
            Required = Required.Default)]
        public string Moduluos { get; set; }

        /// <summary>
        /// The unique identifier for the key. Mapped from <c>kid</c>.
        /// </summary>
        [JsonProperty(
            DefaultValueHandling = DefaultValueHandling.Ignore,
            NullValueHandling = NullValueHandling.Ignore,
            PropertyName = "kid",
            Required = Required.Default)]
        public string UniqueIdentifier { get; set; }

        /// <summary>
        /// The thumbprint of the x.509 certificate, SHA-1 thumbprint. Mapped from <c>x5t</c>.
        /// </summary>
        [JsonProperty(
            DefaultValueHandling = DefaultValueHandling.Ignore,
            NullValueHandling = NullValueHandling.Ignore,
            PropertyName = "x5t",
            Required = Required.Default)]
        public string Thumbprint { get; set; }
    }
}
