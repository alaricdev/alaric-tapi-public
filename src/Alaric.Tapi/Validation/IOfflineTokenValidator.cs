﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Alaric.Tapi.Validation
{
    public interface IOfflineTokenValidator
    {
        Task<bool> ValidateAccessTokenAsync(string token);
    }
}
