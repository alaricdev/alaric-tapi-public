﻿namespace Alaric.Tapi
{
    public static class StringExtensions
    {
        public static string EnsureLeadingSlash(this string url)
        {
            if (url?.StartsWith("/") == false)
            {
                return "/" + url;
            }

            return url;
        }

        public static string EnsureTrailingSlash(this string url)
        {
            if (url?.EndsWith("/") == false)
            {
                return url + "/";
            }

            return url;
        }

        public static string RemoveLeadingSlash(this string url)
        {
            if (url?.StartsWith("/") == true)
            {
                url = url.Substring(1);
            }

            return url;
        }

        public static string RemoveTrailingSlash(this string url)
        {
            if (url?.EndsWith("/") == true)
            {
                url = url.Substring(0, url.Length - 1);
            }

            return url;
        }

        public static string RemoveScheme(this string url)
        {
            if (url?.StartsWith("http://") == true)
            {
                url = url.Remove(0, 7);
            }
            else if (url?.StartsWith("https://") == true)
            {
                url = url.Remove(0, 8);
            }

            return url;
        }
    }
}
